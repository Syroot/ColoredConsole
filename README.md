A small one-class-library allowing you to quickly interleave colors with your `ColorConsole.Write` and
`ColorConsole.WriteLine` commands to render them in full color with one call. Requires VT support of Windows 10.

```cs
using System.Drawing;
using Syroot.ColoredConsole;

class Program
{
    static void Main()
    {
        // Interleave argument list with System.Drawing.Color instances to color following text.
        ColorConsole.WriteLine(Color.Red, "This is red! ", Color.Green, "This is green!");

        // Return value is the text without any color or VT commands to log it away elsewhere.
        string plainText = ColorConsole.WriteLine("Normal color. ", Color.Magenta, "Magenta!");
        System.Diagnostics.Debug.WriteLine(plainText);
    }
}
```

Unsupported systems will simply write uncolored text.
