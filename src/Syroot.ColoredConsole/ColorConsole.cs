﻿using System.Collections;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using Windows.Win32;
using Windows.Win32.Foundation;
using Windows.Win32.System.Console;

namespace Syroot.ColoredConsole
{
    /// <summary>
    /// Represents a system console using virtual terminal (VT) codes to allow 24-bit coloring of text, if supported.
    /// </summary>
    public static class ColorConsole
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        // See https://docs.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences#text-formatting
        private const char _esc = '\u001b';
        private const char _sgrNew = '[';
        private const string _sgrSep = ";"; // not a char to support .NET Standard 2.0 String.Join()
        private const char _sgrEnd = 'm';
        private const byte _sgrDefault = 0;
        private const byte _sgrForegroundExtended = 38;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly bool _supported = CheckColorSupport();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Writes the specified arguments to the standard output stream. Interleaved <see cref="Color"/> parameters
        /// change the color of following text.
        /// </summary>
        /// <param name="args">The arguments to write. Flattens multi-level arrays and ignores <see langword="null"/>.</param>
        /// <returns>The text without any virtual terminal commands.</returns>
        public static string Write(params object[] args)
        {
            (string plainText, string? vtText) = BuildText(args);
            Console.Write(vtText ?? plainText);
            return plainText;
        }

        /// <summary>
        /// Writes the specified arguments, followed by the current line terminator, to the standard output stream.
        /// Interleaved <see cref="Color"/> parameters change the color of following text.
        /// </summary>
        /// <param name="args">The arguments to write. Flattens multi-level arrays and ignores <see langword="null"/>.</param>
        /// <returns>The text without any virtual terminal commands.</returns>
        public static string WriteLine(params object[] args)
        {
            (string plainText, string? vtText) = BuildText(args);
            Console.WriteLine(vtText ?? plainText);
            return plainText;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static (string plainText, string? vtText) BuildText(params object[] args)
        {
            // Build a VT string for the console.
            StringBuilder text = new();
            StringBuilder? vt = _supported ? new StringBuilder() : null;

            void writeArgs(IEnumerable args)
            {
                // Apply options inlined in the given parameters.
                foreach (object? arg in args)
                {
                    switch (arg)
                    {
                        case Color color:
                            vt?.Append(_esc).Append(_sgrNew)
                                .Append(String.Join(_sgrSep, _sgrForegroundExtended, 2, color.R, color.G, color.B))
                                .Append(_sgrEnd);
                            break;
                        case IEnumerable enumerable:
                            writeArgs(enumerable);
                            break;
                        case null:
                            break;
                        default:
                            text.Append(arg);
                            vt?.Append(arg);
                            break;
                    }
                }
            }
            writeArgs(args);

            // Reset any options.
            vt?.Append(_esc).Append(_sgrNew).Append(_sgrDefault).Append(_sgrEnd);

            // Return the final text.
            return (text.ToString(), vt?.ToString());
        }

        private static unsafe bool CheckColorSupport()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                return true;
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows) && Environment.OSVersion.Version.Major >= 10)
            {
                // Manually enable VT support in Windows 10.
                HANDLE outputHandle = PInvoke.GetStdHandle(STD_HANDLE.STD_OUTPUT_HANDLE);
                if (outputHandle == PInvoke.INVALID_HANDLE_VALUE)
                    return false;

                CONSOLE_MODE outputMode;
                if (!PInvoke.GetConsoleMode(outputHandle, &outputMode))
                    return false;
                if (!PInvoke.SetConsoleMode(outputHandle, outputMode | CONSOLE_MODE.ENABLE_VIRTUAL_TERMINAL_PROCESSING))
                    return false;

                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
